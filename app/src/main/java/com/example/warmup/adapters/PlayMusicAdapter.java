package com.example.warmup.adapters;

import android.app.AlertDialog;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;

import com.example.warmup.ChoosePlaylistList;
import com.example.warmup.R;
import com.example.warmup.fragments.PlayMusicFragment;
import com.example.warmup.fragments.PlaylistsFragment;
import com.example.warmup.models.RecentlyPlayedModel;

import java.util.ArrayList;

public class PlayMusicAdapter extends RecyclerView.Adapter<PlayMusicAdapter.PlayMusicViewHolder> {

    private ArrayList<RecentlyPlayedModel> recyclerData;
    private PlayMusicFragment playMusicFragment;
    private PlaylistsFragment playlistsFragment;
    int currentIndex;

    public PlayMusicAdapter(ArrayList<RecentlyPlayedModel> recyclerData, int currentIndex, PlayMusicFragment playMusicFragment, PlaylistsFragment playlistsFragment) {
        this.recyclerData = recyclerData;
        this.currentIndex = currentIndex;
        this.playMusicFragment = playMusicFragment;
        this.playlistsFragment = playlistsFragment;
    }

    @NonNull
    @Override
    public PlayMusicViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.play_music_card_view, parent, false);
        return new PlayMusicViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull PlayMusicAdapter.PlayMusicViewHolder holder, int position) {
        RecentlyPlayedModel recentlyPlayedModel = recyclerData.get(position);
        holder.musicThumbnail.setImageResource(recentlyPlayedModel.getRecentlyPlayedImage());
        holder.musicTitle.setText(recentlyPlayedModel.getRecentlyPlayedTitle());
        holder.musicDuration.setText(recentlyPlayedModel.getRecentlyPlayedDuration());
        if (position == currentIndex) {
            holder.musicTitle.setTextColor(Color.RED);
        } else {
            holder.musicTitle.setTextColor(Color.WHITE);
        }
    }

    @Override
    public int getItemCount() {
        return recyclerData.size();
    }

    public class PlayMusicViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private AppCompatTextView musicTitle;
        private AppCompatTextView musicDuration;
        public AppCompatImageView musicThumbnail;
        public AppCompatImageView moreActionsBtn;

        public PlayMusicViewHolder(View view) {
            super(view);
            musicTitle = view.findViewById(R.id.music_title);
            musicDuration = view.findViewById(R.id.music_duration);
            musicThumbnail = view.findViewById(R.id.music_thumbnail);
            moreActionsBtn = view.findViewById(R.id.more_actions_btn);
            moreActionsBtn.setOnClickListener(this);
            musicDuration.setOnClickListener(this);
            musicTitle.setOnClickListener(this);
            musicThumbnail.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view.getId() == musicTitle.getId() || view.getId() == musicDuration.getId() || view.getId() == musicThumbnail.getId()) {
                currentIndex = getAdapterPosition();
                playMusicFragment.updateViews(currentIndex);
                notifyDataSetChanged();
            } else if (view.getId() == moreActionsBtn.getId()) {
                PopupMenu popup = new PopupMenu(playMusicFragment.getContext(), view);
                popup.getMenuInflater().inflate(R.menu.song_popup_menu, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getTitle().equals("Add To Playlist")) {
                            addSongToPlaylist(recyclerData.get(getAdapterPosition()));
                        } else if (item.getTitle().equals("Maybe")) {
                            maybe();
                        }
                        return true;
                    }
                });
                popup.show();
            }
        }

        private void maybe() {
            //do nothing for the moment
        }

        private void addSongToPlaylist(RecentlyPlayedModel song) {
            new ChoosePlaylistList(playMusicFragment.getContext(), playMusicFragment.getActivity(), song, playlistsFragment);
        }
    }
}
