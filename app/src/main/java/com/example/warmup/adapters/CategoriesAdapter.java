package com.example.warmup.adapters;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.warmup.MainActivity;
import com.example.warmup.fragments.MusicListFragment;
import com.example.warmup.models.CategoriesModel;
import com.example.warmup.R;

import java.util.ArrayList;

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.CardViewHolder> {

    private ArrayList<CategoriesModel> recyclerData;
    private FragmentManager fragmentManager;

    public CategoriesAdapter(ArrayList<CategoriesModel> recyclerData, Activity activity) {
        this.recyclerData = recyclerData;
        fragmentManager = ((MainActivity) activity).getManager();
    }


    @NonNull
    @Override
    public CardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.categories_card_view, parent, false);
        return new CardViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull CardViewHolder holder, int position) {
        CategoriesModel categoriesModel = recyclerData.get(position);
        holder.categoryName.setText(categoriesModel.getCategoryName());
        holder.categoryThumbnail.setImageResource(categoriesModel.getCategoryPhoto());
    }


    @Override
    public int getItemCount() {
        return recyclerData.size();
    }

    public class CardViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private AppCompatTextView categoryName;
        public AppCompatImageView categoryThumbnail;
        public ConstraintLayout categoriesCard;

        public CardViewHolder(View view) {
            super(view);
            categoryName = view.findViewById(R.id.category_name);
            categoryThumbnail = view.findViewById(R.id.category_image);
            categoriesCard = view.findViewById(R.id.card_layout_categories);
            categoriesCard.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view.getId() == categoriesCard.getId()){
                String musicType = recyclerData.get(getAdapterPosition()).getCategoryName();
                MusicListFragment musicListFragment = new MusicListFragment();
                Bundle args = new Bundle();
                args.putSerializable("music_type", musicType);
                musicListFragment.setArguments(args);
                fragmentManager.beginTransaction().add(R.id.main_container, musicListFragment).commit();
            }
        }
    }
}
