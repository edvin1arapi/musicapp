package com.example.warmup.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;

import android.text.InputType;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;

import com.example.warmup.database.DatabaseHelper;
import com.example.warmup.models.PlaylistModel;
import com.example.warmup.R;

import java.util.ArrayList;

public class PlaylistAdapter extends RecyclerView.Adapter<PlaylistAdapter.PlaylistViewHolder> {

    private ArrayList<PlaylistModel> recyclerData;
    private Context context;

    public PlaylistAdapter(Context context, ArrayList<PlaylistModel> recyclerData) {
        this.recyclerData = recyclerData;
        this.context = context;
    }


    @NonNull
    @Override
    public PlaylistAdapter.PlaylistViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.playlist_card_view, parent, false);
        return new PlaylistAdapter.PlaylistViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull PlaylistAdapter.PlaylistViewHolder holder, int position) {
        PlaylistModel playlistModel = recyclerData.get(position);
        holder.playlistName.setText(playlistModel.getPlaylistName());
        holder.playlistSongsNumber.setText(String.valueOf(playlistModel.getPlaylistSongsNumber()));
    }


    @Override
    public int getItemCount() {
        return recyclerData.size();
    }


    public class PlaylistViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private AppCompatTextView playlistName;
        private AppCompatTextView playlistSongsNumber;
        private AppCompatImageView moreButton;

        public PlaylistViewHolder(View view) {
            super(view);
            playlistName = view.findViewById(R.id.playlist_title);
            playlistSongsNumber = view.findViewById(R.id.playlist_song_number);
            moreButton = view.findViewById(R.id.more_actions_playlist);
            moreButton.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view.getId() == moreButton.getId()) {
                PopupMenu popup = new PopupMenu(context, view);
                popup.getMenuInflater().inflate(R.menu.playlist_popup_menu, popup.getMenu());
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        if (item.getTitle().equals("Rename Playlist")) {
                            updatePlaylistName();
                        } else if (item.getTitle().equals("Delete Playlist")) {
                            deletePlaylist();
                        }
                        return true;
                    }
                });
                popup.show();
            }
        }

        private void deletePlaylist() {
            deletePlaylistFromDatabase();
            recyclerData.remove(getAdapterPosition());
            notifyDataSetChanged();
        }

        private void deletePlaylistFromDatabase() {
            DatabaseHelper databaseHelper = new DatabaseHelper(context);
            SQLiteDatabase database = databaseHelper.getWritableDatabase();
            databaseHelper.deletePlaylist(recyclerData.get(getAdapterPosition()).getPlaylistId(), database);
            databaseHelper.close();
        }

        private void updatePlaylistName() {
            showInputDialog();
        }

        private void showInputDialog() {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Please set the playlist name..");

            final EditText input = new EditText(context);
            input.setText(recyclerData.get(getAdapterPosition()).getPlaylistName(), TextView.BufferType.EDITABLE);
            input.setInputType(InputType.TYPE_CLASS_TEXT);
            builder.setView(input);
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String newTitle = input.getText().toString();
                    updatePlaylistNameOnDatabase(newTitle);
                    recyclerData.get(getAdapterPosition()).setPlaylistName(newTitle);
                    notifyDataSetChanged();
                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            builder.show();
        }

        private void updatePlaylistNameOnDatabase(String newTitle) {
            DatabaseHelper databaseHelper = new DatabaseHelper(context);
            SQLiteDatabase database = databaseHelper.getWritableDatabase();
            databaseHelper.updatePlaylistTitle(recyclerData.get(getAdapterPosition()).getPlaylistId(), newTitle, database);
            databaseHelper.close();
        }
    }
}
