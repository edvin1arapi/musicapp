package com.example.warmup.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.warmup.R;
import com.example.warmup.models.RecentlyPlayedModel;

import java.util.ArrayList;

public class MusicListAdapter extends RecyclerView.Adapter<MusicListAdapter.MusiclistViewHolder> {

    private ArrayList<RecentlyPlayedModel> recyclerData;

    public MusicListAdapter(ArrayList<RecentlyPlayedModel> recyclerData) {
        this.recyclerData = recyclerData;
    }


    @NonNull
    @Override
    public MusicListAdapter.MusiclistViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.play_music_card_view, parent, false);
        return new MusicListAdapter.MusiclistViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MusicListAdapter.MusiclistViewHolder holder, int position) {
        RecentlyPlayedModel recentlyPlayedModel = recyclerData.get(position);
        holder.musicTitle.setText(recentlyPlayedModel.getRecentlyPlayedTitle());
        holder.musicDuration.setText(String.valueOf(recentlyPlayedModel.getRecentlyPlayedDuration()));
        holder.musicThumbnail.setImageResource(recentlyPlayedModel.getRecentlyPlayedImage());

    }


    @Override
    public int getItemCount() {
        return recyclerData.size();
    }


    public class MusiclistViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private AppCompatTextView musicTitle;
        private AppCompatTextView musicDuration;
        public AppCompatImageView musicThumbnail;
        public AppCompatImageView moreActionsBtn;

        public MusiclistViewHolder(View view) {
            super(view);
            musicTitle = view.findViewById(R.id.music_title);
            musicDuration = view.findViewById(R.id.music_duration);
            musicThumbnail = view.findViewById(R.id.music_thumbnail);
            musicTitle.setOnClickListener(this);
            musicDuration.setOnClickListener(this);
            musicThumbnail.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view.getId() == musicTitle.getId()){

            }
        }
    }
}
