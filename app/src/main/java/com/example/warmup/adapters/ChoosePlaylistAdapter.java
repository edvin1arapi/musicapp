package com.example.warmup.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.warmup.R;
import com.example.warmup.models.PlaylistModel;

import java.util.ArrayList;

public class ChoosePlaylistAdapter extends RecyclerView.Adapter<ChoosePlaylistAdapter.ChoosePlaylistViewHolder> {

    private ArrayList<PlaylistModel> playlists;

    public ChoosePlaylistAdapter(ArrayList<PlaylistModel> playlists) {
        this.playlists = playlists;
    }

    @NonNull
    @Override
    public ChoosePlaylistViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.choose_playlist_card_view, parent, false);
        return new ChoosePlaylistAdapter.ChoosePlaylistViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ChoosePlaylistViewHolder holder, int position) {
        PlaylistModel playlistModel = playlists.get(position);
        holder.playlistTitle.setText(playlistModel.getPlaylistName());
        holder.playlistSongsNr.setText(String.valueOf(playlistModel.getPlaylistSongsNumber()));
    }

    @Override
    public int getItemCount() {
        return playlists.size();
    }

    public class ChoosePlaylistViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private AppCompatTextView playlistTitle;
        private AppCompatTextView playlistSongsNr;
        private AppCompatButton addToPlaylistBtn;

        public ChoosePlaylistViewHolder(@NonNull View itemView) {
            super(itemView);
            playlistTitle = itemView.findViewById(R.id.choose_playlist_name);
            playlistSongsNr = itemView.findViewById(R.id.choose_playlist_song_number);
            addToPlaylistBtn = itemView.findViewById(R.id.add_to_playlist_btn);
            addToPlaylistBtn.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view.getId() == addToPlaylistBtn.getId()){
                onConfirmListener.onClick(view, playlists.get(getAdapterPosition()).getPlaylistId());
            }
        }
    }

    public void setOnConfirmListener(OnConfirmListener listener) {
        onConfirmListener = listener;
    }

    private OnConfirmListener onConfirmListener;


    public interface OnConfirmListener {

        void onClick(View v, int playlistId);
    }
}
