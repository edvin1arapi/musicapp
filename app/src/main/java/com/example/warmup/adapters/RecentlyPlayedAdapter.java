package com.example.warmup.adapters;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.warmup.MainActivity;
import com.example.warmup.R;
import com.example.warmup.fragments.PlayMusicFragment;
import com.example.warmup.fragments.PlaylistsFragment;
import com.example.warmup.models.RecentlyPlayedModel;

import java.util.ArrayList;

public class RecentlyPlayedAdapter extends RecyclerView.Adapter<RecentlyPlayedAdapter.RecentlyPlayedViewHolder> {

    private ArrayList<RecentlyPlayedModel> recyclerData;
    private PlayMusicFragment dialog;
    private Bundle args = new Bundle();
    private AppCompatActivity activity;
    private PlaylistsFragment playlistsFragment;

    public RecentlyPlayedAdapter(ArrayList<RecentlyPlayedModel> recyclerData, PlaylistsFragment playlistsFragment) {
        this.recyclerData = recyclerData;
        this.playlistsFragment = playlistsFragment;
        dialog = new PlayMusicFragment(playlistsFragment);
    }

    @NonNull
    @Override
    public RecentlyPlayedViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recently_played_card_view, parent, false);
        return new RecentlyPlayedViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecentlyPlayedAdapter.RecentlyPlayedViewHolder holder, int position) {
        RecentlyPlayedModel recentlyPlayedModel = recyclerData.get(position);
        holder.recentlyPlayedThumbnail.setImageResource(recentlyPlayedModel.getRecentlyPlayedImage());
        holder.recentlyPlayedTitle.setText(recentlyPlayedModel.getRecentlyPlayedTitle());
        holder.recentlyPlayedDuration.setText(recentlyPlayedModel.getRecentlyPlayedDuration());
    }

    @Override
    public int getItemCount() {
        return recyclerData.size();
    }

    public class RecentlyPlayedViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private AppCompatTextView recentlyPlayedTitle;
        private AppCompatTextView recentlyPlayedDuration;
        public AppCompatImageView recentlyPlayedThumbnail;
        public AppCompatImageView recentlyPlayedBtnPlay;

        public RecentlyPlayedViewHolder(View view) {
            super(view);
            recentlyPlayedTitle = view.findViewById(R.id.recently_played_title);
            recentlyPlayedDuration = view.findViewById(R.id.recently_played_duration);
            recentlyPlayedThumbnail = view.findViewById(R.id.recently_played_thumbnail);
            recentlyPlayedBtnPlay = view.findViewById(R.id.recently_played_play_btn);
            recentlyPlayedBtnPlay.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (view.getId() == recentlyPlayedBtnPlay.getId()) {
                System.out.println("ooooooooo" + recyclerData.get(getAdapterPosition()).getRecentlyPlayedDuration() + recyclerData.get(getAdapterPosition()).getRecentlyPlayedTitle());
                activity = (AppCompatActivity) view.getContext();
                if (!((MainActivity) activity).isDialogShown()) {
                    args.putSerializable("key", recyclerData);
                    args.putString("category", "Recently Played");
                    args.putInt("currentSongIndex", getAdapterPosition());
                    dialog.setArguments(args);
                    dialog.show(activity.getSupportFragmentManager(), "");
                } else {
                    dialog.setUpMusicRecycler(getAdapterPosition());
                    dialog.updateViews(getAdapterPosition());
                }
            }
        }
    }
}
