package com.example.warmup.fragments;

import android.content.res.Resources;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.warmup.models.CategoriesModel;
import com.example.warmup.GridSpacingItemDecoration;
import com.example.warmup.R;
import com.example.warmup.models.RecentlyPlayedModel;
import com.example.warmup.adapters.CategoriesAdapter;
import com.example.warmup.adapters.RecentlyPlayedAdapter;

import java.util.ArrayList;

public class ExploreFragment extends Fragment implements View.OnClickListener {

    private String theme = "White";
    private ConstraintLayout rootLayout;
    private CategoriesAdapter categoriesAdapter;
    private PlaylistsFragment playlistsFragment;
    private RecentlyPlayedAdapter recentlyPlayedAdapter;
    private ArrayList<CategoriesModel> categoriesData;
    private ArrayList<RecentlyPlayedModel> recentlyPlayedData;
    private RecyclerView categoriesRecycler;
    private RecyclerView recentlyPlayedRecycler;

    public ExploreFragment() {
    }

    public ExploreFragment(PlaylistsFragment playlistsFragment) {
        this.playlistsFragment = playlistsFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_explore, container, false);
        findViews(view);
        setOnClickListeners();
        SetupRecyclerViews();
        setTheme();
        System.out.println("explore");
        return view;
    }

    private void findViews(View view) {
        rootLayout = view.findViewById(R.id.blank_fragment);
        categoriesRecycler = view.findViewById(R.id.categories_recycler);
        recentlyPlayedRecycler = view.findViewById(R.id.recently_played_recycler);
    }

    private void SetupRecyclerViews() {
        setupCategoriesRecyclerView();
        setupRecentlyPlayedRecyclerView();

    }

    private void setupRecentlyPlayedRecyclerView() {
        recentlyPlayedData = new ArrayList<>();
        recentlyPlayedAdapter = new RecentlyPlayedAdapter(recentlyPlayedData, playlistsFragment);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        ((LinearLayoutManager) layoutManager).setOrientation(RecyclerView.HORIZONTAL);
        recentlyPlayedRecycler.setLayoutManager(layoutManager);
        recentlyPlayedRecycler.setItemAnimator(new DefaultItemAnimator());
        recentlyPlayedRecycler.setAdapter(recentlyPlayedAdapter);
        getRecentlyPlayedData();
    }

    private void setupCategoriesRecyclerView() {
        categoriesData = new ArrayList<>();
        categoriesAdapter = new CategoriesAdapter(categoriesData, getActivity());
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getContext(), 2);
        categoriesRecycler.setLayoutManager(mLayoutManager);
        categoriesRecycler.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        categoriesRecycler.setItemAnimator(new DefaultItemAnimator());
        categoriesRecycler.setAdapter(categoriesAdapter);
        getCategoriesData();
    }


    private void setOnClickListeners() {

    }

    private void setTheme() {
        if (theme.equals("White theme")) {
            rootLayout.setBackgroundColor(getResources().getColor(R.color.color_white));
        } else {
            rootLayout.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        }
    }

    @Override
    public void onClick(View view) {
       /*if (view.getId() == backBtn.getId()) {
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            fragmentManager.beginTransaction().remove(this).commit();
        }*/
    }

    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    private void getRecentlyPlayedData() {
        int[] recentlyPlayedcovers = new int[]{
                R.drawable.ic_one_24dp,
                R.drawable.ic_two_24dp,
                R.drawable.ic_three_24dp,
                R.drawable.ic_four_24dp,
                R.drawable.ic_five_24dp,
                R.drawable.ic_six_24dp,
                R.drawable.ic_seven_24dp};

        RecentlyPlayedModel recentlyPlayed = new RecentlyPlayedModel(recentlyPlayedcovers[0], "French Montana - Writing on the Wall ft. Post Malone, Cardi B, Rvssian", "3:15");
        recentlyPlayedData.add(recentlyPlayed);
        recentlyPlayed = new RecentlyPlayedModel(recentlyPlayedcovers[1], "Post Malone - Congratulations ft. Quavo", "3:33");
        recentlyPlayedData.add(recentlyPlayed);
        recentlyPlayed = new RecentlyPlayedModel(recentlyPlayedcovers[2], "Post Malone - Goodbyes ft. Young Thug (Rated PG)", "4:56");
        recentlyPlayedData.add(recentlyPlayed);
        recentlyPlayed = new RecentlyPlayedModel(recentlyPlayedcovers[3], "French Montana - Unforgettable ft. Swae Lee (Official Music Video)", "2:59");
        recentlyPlayedData.add(recentlyPlayed);
        recentlyPlayed = new RecentlyPlayedModel(recentlyPlayedcovers[4], "Future - Mask Off (Official Music Video)", "3:11");
        recentlyPlayedData.add(recentlyPlayed);
        recentlyPlayed = new RecentlyPlayedModel(recentlyPlayedcovers[5], "Childish Gambino - This Is America (Official Video)", "2:58");
        recentlyPlayedData.add(recentlyPlayed);
        recentlyPlayed = new RecentlyPlayedModel(recentlyPlayedcovers[6], "KILLSHOT [Official Audio]", "3:46");
        recentlyPlayedData.add(recentlyPlayed);
        recentlyPlayedAdapter.notifyDataSetChanged();
    }

    private void getCategoriesData() {
        int[] categoriesCovers = new int[]{
                R.drawable.ic_one_24dp,
                R.drawable.ic_two_24dp,
                R.drawable.ic_three_24dp,
                R.drawable.ic_four_24dp,
                R.drawable.ic_five_24dp,
                R.drawable.ic_six_24dp,
                R.drawable.ic_seven_24dp};

        CategoriesModel categoriesModel = new CategoriesModel("POP", categoriesCovers[0]);
        categoriesData.add(categoriesModel);
        categoriesModel = new CategoriesModel("ROCK", categoriesCovers[1]);
        categoriesData.add(categoriesModel);
        categoriesModel = new CategoriesModel("INDIE", categoriesCovers[2]);
        categoriesData.add(categoriesModel);
        categoriesModel = new CategoriesModel("DANCE/ELECTRONIC", categoriesCovers[3]);
        categoriesData.add(categoriesModel);
        categoriesModel = new CategoriesModel("NEW RELEASES", categoriesCovers[4]);
        categoriesData.add(categoriesModel);
        categoriesModel = new CategoriesModel("HIP-HOP", categoriesCovers[5]);
        categoriesData.add(categoriesModel);
        categoriesModel = new CategoriesModel("CHARTS", categoriesCovers[6]);
        categoriesData.add(categoriesModel);
        categoriesModel = new CategoriesModel("PODCASTS", categoriesCovers[6]);
        categoriesData.add(categoriesModel);
        categoriesAdapter.notifyDataSetChanged();
    }
}