package com.example.warmup.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.example.warmup.R;
import com.example.warmup.adapters.PlayMusicAdapter;
import com.example.warmup.models.RecentlyPlayedModel;

import java.util.ArrayList;

public class PlayMusicFragment extends DialogFragment implements android.view.View.OnClickListener{

    private ConstraintLayout minimizedLayout;
    private ConstraintLayout playMusicLayout;

    private PlayMusicAdapter playMusicAdapter;

    private RecyclerView musicItemRecycler;
    private AppCompatTextView title;
    private AppCompatImageView backArrow;
    private AppCompatImageView closeBtn;
    private AppCompatImageView closeBtnMinimized;
    private AppCompatImageView expandBtn;
    private AppCompatTextView timePassed;
    private AppCompatTextView timeLength;
    private AppCompatTextView titlePlayingNow;
    private AppCompatTextView minimizedTitle;
    private AppCompatImageView minimizedThumbnail;
    private PlaylistsFragment playlistsFragment;

    private ArrayList<RecentlyPlayedModel> musicData = new ArrayList<>();
    private String titleText;
    private int currentIndexPlaying;

    private Communicator communicator;

    public interface Communicator {
        void doChanges(int end, boolean shown);
    }

    public PlayMusicFragment() {
    }

    public PlayMusicFragment(PlaylistsFragment playlistsFragment) {
        this.playlistsFragment = playlistsFragment;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            communicator = (Communicator) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement TextClicked");
        }
    }

    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Context context = getActivity();
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = LayoutInflater.from(context);
        View dialogView = inflater.inflate(R.layout.fragment_play_music, null);
        findViews(dialogView);
        setOnClickListeners();
        getArgs();
        setUpMusicRecycler(currentIndexPlaying);
        updateViews(currentIndexPlaying);
        builder.setView(dialogView);
        return builder.create();
    }

    private void getArgs() {
        assert getArguments() != null;
        titleText = getArguments().getString("category");
        musicData = (ArrayList<RecentlyPlayedModel>) getArguments().getSerializable("key");
        currentIndexPlaying = getArguments().getInt("currentSongIndex");
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getDialog().setCanceledOnTouchOutside(false);
        communicator.doChanges(300, true);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    private void setOnClickListeners() {
        backArrow.setOnClickListener(this);
        closeBtn.setOnClickListener(this);
        closeBtnMinimized.setOnClickListener(this);
        expandBtn.setOnClickListener(this);
    }

    private void findViews(View view) {
        playMusicLayout = view.findViewById(R.id.play_music_layout);
        minimizedLayout = view.findViewById(R.id.minimized_layout);
        musicItemRecycler = view.findViewById(R.id.play_music_recycler);
        title = view.findViewById(R.id.category_or_playlist_name);
        backArrow = view.findViewById(R.id.back_arrow);
        closeBtn = view.findViewById(R.id.close_btn);
        closeBtnMinimized = view.findViewById(R.id.close_btn_minimized);
        expandBtn = view.findViewById(R.id.expand_btn);
        timePassed = view.findViewById(R.id.time_passed);
        timeLength = view.findViewById(R.id.time_length);
        titlePlayingNow = view.findViewById(R.id.title_playing_now);
        minimizedTitle = view.findViewById(R.id.minimized_title);
        minimizedThumbnail = view.findViewById(R.id.minimized_thumbnail);
    }

    public void setUpMusicRecycler(int currentIndexPlaying) {
        title.setText(titleText);
        playMusicAdapter = new PlayMusicAdapter(musicData, currentIndexPlaying, this, playlistsFragment);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        musicItemRecycler.setLayoutManager(layoutManager);
        musicItemRecycler.setItemAnimator(new DefaultItemAnimator());
        musicItemRecycler.setAdapter(playMusicAdapter);
        musicItemRecycler.scrollToPosition(currentIndexPlaying);
    }

    public void updateViews(int currentIndexPlaying) {
        timeLength.setText(musicData.get(currentIndexPlaying).getRecentlyPlayedDuration());
        titlePlayingNow.setText(musicData.get(currentIndexPlaying).getRecentlyPlayedTitle());
        minimizedTitle.setText(musicData.get(currentIndexPlaying).getRecentlyPlayedTitle());
        minimizedThumbnail.setImageResource(musicData.get(currentIndexPlaying).getRecentlyPlayedImage());
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == closeBtn.getId() || view.getId() == closeBtnMinimized.getId()) {
            System.out.println("oooooosaas");
            getDialog().dismiss();
            communicator.doChanges(10, false);
        } else if (view.getId() == backArrow.getId()) {
            showMinimizedMode();
        } else if (view.getId() == expandBtn.getId()) {
            showMaximizedMode();
        }
    }

    private void showMaximizedMode() {
        minimizedLayout.setVisibility(View.GONE);
        playMusicLayout.setVisibility(View.VISIBLE);
        getDialog().getWindow().setFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND, WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        WindowManager.LayoutParams layoutParams = getDialog().getWindow().getAttributes();
        layoutParams.gravity = Gravity.CENTER;
        layoutParams.x = 0;
        layoutParams.y = 0;
        layoutParams.width = ViewGroup.LayoutParams.WRAP_CONTENT;
        getDialog().getWindow().setAttributes(layoutParams);
    }

    private void showMinimizedMode() {
        minimizedLayout.setVisibility(View.VISIBLE);
        playMusicLayout.setVisibility(View.GONE);
        getDialog().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        getDialog().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE);
        WindowManager.LayoutParams wmlp = getDialog().getWindow().getAttributes();
        wmlp.gravity = Gravity.BOTTOM;
        wmlp.y = 170;
        getDialog().getWindow().setAttributes(wmlp);
    }

}
