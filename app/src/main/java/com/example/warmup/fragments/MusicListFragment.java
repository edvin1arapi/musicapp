package com.example.warmup.fragments;


import android.os.Bundle;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.warmup.R;
import com.example.warmup.adapters.MusicListAdapter;
import com.example.warmup.models.RecentlyPlayedModel;

import java.util.ArrayList;

public class MusicListFragment extends Fragment implements View.OnClickListener {

    private AppCompatTextView musicType;
    private AppCompatImageView shufleBtn;
    private AppCompatImageView listBtn;
    private AppCompatImageView closeBtn;
    private RecyclerView musicListRecycler;
    private ArrayList<RecentlyPlayedModel> musicList;
    private MusicListAdapter musicListAdapter;

    public MusicListFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_music_list, container, false);
        findViews(view);
        setOnClickListeners();
        getParams();
        setupRecycler();
        return view;
    }

    private void setOnClickListeners() {
        closeBtn.setOnClickListener(this);
    }

    private void getParams() {
        String type = getArguments().getString("music_type");
        musicType.setText(type);

        //kerko ne database me emer kategorie type.
        //mbush listen musicList me rezultatet e db.
    }


    private void findViews(View view){
        musicType = view.findViewById(R.id.music_type);
        shufleBtn = view.findViewById(R.id.music_list_shufle_btn);
        listBtn = view.findViewById(R.id.music_list_list_btn);
        closeBtn = view.findViewById(R.id.close_category_btn);
        musicListRecycler = view.findViewById(R.id.music_list_recycler);
        //TODO: musicList to be passed as a argument from CategoriesAdapter
        musicList = new ArrayList<>();
        RecentlyPlayedModel recentlyPlayedModel = new RecentlyPlayedModel(R.drawable.ic_five_24dp,"category", "02:58");
        musicList.add(recentlyPlayedModel);
    }

    private void setupRecycler() {
        musicListAdapter = new MusicListAdapter(musicList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        ((LinearLayoutManager) layoutManager).setOrientation(RecyclerView.VERTICAL);
        musicListRecycler.setLayoutManager(layoutManager);
        musicListRecycler.setItemAnimator(new DefaultItemAnimator());
        musicListRecycler.setAdapter(musicListAdapter);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == closeBtn.getId()){
            getFragmentManager().beginTransaction().remove(this).commit();
        }
    }
}
