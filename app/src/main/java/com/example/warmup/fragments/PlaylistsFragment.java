package com.example.warmup.fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import androidx.constraintlayout.widget.Guideline;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.example.warmup.database.DatabaseHelper;
import com.example.warmup.database.PlaylistContract;
import com.example.warmup.models.PlaylistModel;
import com.example.warmup.R;
import com.example.warmup.models.RecentlyPlayedModel;
import com.example.warmup.adapters.PlaylistAdapter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

public class PlaylistsFragment extends Fragment implements View.OnClickListener {

    private FloatingActionButton addPlaylistBtn;
    private Guideline guideline;
    private ArrayList<PlaylistModel> playlistList;
    private PlaylistAdapter playlistAdapter;
    private RecyclerView playlistRecycler;

    public PlaylistsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        playlistList = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_playlists, container, false);
        findViews(view);
        addOnClickListeners();
        getPlaylistDataFromDb();
        addAndRefreshPlaylist();
        return view;
    }

    private void getPlaylistDataFromDb() {
        DatabaseHelper databaseHelper = new DatabaseHelper(getContext());
        SQLiteDatabase database = databaseHelper.getReadableDatabase();
        Cursor cursor = databaseHelper.readPlaylist(database);
        while (cursor.moveToNext()) {
            int id = cursor.getInt(cursor.getColumnIndex(PlaylistContract.PlaylistEntry.COLUMN_NAME_ID));
            String title = cursor.getString(cursor.getColumnIndex(PlaylistContract.PlaylistEntry.COLUMN_NAME_TITLE));
            int songs = cursor.getInt(cursor.getColumnIndex(PlaylistContract.PlaylistEntry.COLUMN_NAME_SONGS_NUMBER));
            PlaylistModel playlist = new PlaylistModel(id, title, songs, new ArrayList<RecentlyPlayedModel>());
            playlistList.add(playlist);
        }
        databaseHelper.close();
    }

    private void addOnClickListeners() {
        addPlaylistBtn.setOnClickListener(this);
    }

    private void findViews(View view) {
        addPlaylistBtn = view.findViewById(R.id.add_playlist_btn);
        guideline = view.findViewById(R.id.guideline);
        playlistRecycler = view.findViewById(R.id.playlist_recycler);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == addPlaylistBtn.getId()) {
            showInputDialog();
        }
    }

    private void showInputDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Please set the playlist name..");

        final EditText input = new EditText(getContext());
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String playlistTitle = input.getText().toString();
                addNewPlaylistOnDatabase(playlistTitle);
                refreshPlaylists();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    private void addNewPlaylistOnDatabase(String playlistTitle) {
        if (isPlaylistOnDatabase(playlistTitle)){
            Toast.makeText(getContext(), "This Playlist already exists", Toast.LENGTH_SHORT).show();
        } else {
            DatabaseHelper databaseHelper = new DatabaseHelper(getContext());
            SQLiteDatabase database = databaseHelper.getWritableDatabase();
            databaseHelper.addPlaylist(playlistTitle, "0", database);
            databaseHelper.close();
        }
    }

    private boolean isPlaylistOnDatabase(String playlistTitle) {
        DatabaseHelper databaseHelper = new DatabaseHelper(getContext());
        SQLiteDatabase database = databaseHelper.getReadableDatabase();
        Cursor cursor = databaseHelper.readPlaylist(database);
        while (cursor.moveToNext()) {
            String title = cursor.getString(cursor.getColumnIndex(PlaylistContract.PlaylistEntry.COLUMN_NAME_TITLE));
            if (title.equalsIgnoreCase(playlistTitle)){
                return true;
            }
        }
        databaseHelper.close();
        return false;
    }

    private void addAndRefreshPlaylist() {
        playlistAdapter = new PlaylistAdapter(getContext(), playlistList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        ((LinearLayoutManager) layoutManager).setOrientation(RecyclerView.VERTICAL);
        playlistRecycler.setLayoutManager(layoutManager);
        playlistRecycler.setItemAnimator(new DefaultItemAnimator());
        playlistRecycler.setAdapter(playlistAdapter);
    }

    public void refreshPlaylists(){
        playlistList.clear();
        getPlaylistDataFromDb();
        addAndRefreshPlaylist();
    }

    public void onMusicPlaying(int end) {
        guideline.setGuidelineEnd(end);
    }

    //TODO: never let user to add the same database title
}
