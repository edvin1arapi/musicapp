package com.example.warmup;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.ColorDrawable;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.warmup.adapters.ChoosePlaylistAdapter;
import com.example.warmup.database.DatabaseHelper;
import com.example.warmup.database.PlaylistContract;
import com.example.warmup.database.SongContract;
import com.example.warmup.fragments.PlaylistsFragment;
import com.example.warmup.models.PlaylistModel;
import com.example.warmup.models.RecentlyPlayedModel;

import java.util.ArrayList;

public class ChoosePlaylistList implements View.OnClickListener {

    private Dialog dialog;
    private Context context;
    private Activity activity;
    private AppCompatImageView closeBtn;
    private RecyclerView playlistRecycler;
    private AppCompatTextView title;
    private RecentlyPlayedModel song;
    private ChoosePlaylistAdapter adapter;
    private PlaylistsFragment playlistsFragment;
    private ArrayList<PlaylistModel> playlistList = new ArrayList<>();

    public ChoosePlaylistList(Context context, Activity activity, RecentlyPlayedModel song, PlaylistsFragment playlistsFragment) {
        this.context = context;
        this.activity = activity;
        this.song = song;
        this.playlistsFragment = playlistsFragment;
        getPlaylistListFromDB();
        createDialog();
        initView(dialog);
        setUpList();
    }

    private void createDialog() {
        dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.choose_playlist_list);
        Window window = dialog.getWindow();
        assert window != null;

        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        // The absolute height of the available display size in pixels.
        int displayHeight = displayMetrics.heightPixels;

        WindowManager.LayoutParams wlp = window.getAttributes();
        wlp.gravity = Gravity.BOTTOM;
        wlp.width = WindowManager.LayoutParams.MATCH_PARENT;
        wlp.height = (int) (displayHeight * 0.5f);
        window.setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        window.setAttributes(wlp);
        dialog.show();
    }

    private void getPlaylistListFromDB() {
        DatabaseHelper databaseHelper = new DatabaseHelper(context);
        SQLiteDatabase database = databaseHelper.getReadableDatabase();
        Cursor cursor = databaseHelper.readPlaylist(database);
        while (cursor.moveToNext()) {
            int playlistId = cursor.getInt(cursor.getColumnIndex(PlaylistContract.PlaylistEntry.COLUMN_NAME_ID));
            String title = cursor.getString(cursor.getColumnIndex(PlaylistContract.PlaylistEntry.COLUMN_NAME_TITLE));
            int songs = cursor.getInt(cursor.getColumnIndex(PlaylistContract.PlaylistEntry.COLUMN_NAME_SONGS_NUMBER));
            PlaylistModel playlist = new PlaylistModel(playlistId, title, songs, new ArrayList<RecentlyPlayedModel>());
            playlistList.add(playlist);
        }
        databaseHelper.close();
    }

    private void initView(Dialog dialog) {
        closeBtn = dialog.findViewById(R.id.choose_playlist_close);
        playlistRecycler = dialog.findViewById(R.id.choose_playlist_recycler);
        title = dialog.findViewById(R.id.choose_playlist_title);
        closeBtn.setOnClickListener(this);
        title.setText("Add " + song.getRecentlyPlayedTitle() + " to: ");
    }

    private void setUpList() {
        playlistRecycler.setHasFixedSize(true);
        playlistRecycler.setLayoutManager(new LinearLayoutManager(dialog.getContext(), RecyclerView.VERTICAL, false));
        adapter = new ChoosePlaylistAdapter(playlistList);
        adapter.setOnConfirmListener(onConfirmListener);
        playlistRecycler.setAdapter(adapter);
    }

    private ChoosePlaylistAdapter.OnConfirmListener onConfirmListener = new ChoosePlaylistAdapter.OnConfirmListener() {
        @Override
        public void onClick(View v, int playlistId) {
            addSongOnDatabase(playlistId);
        }
    };

    private void addSongOnDatabase(int playlistId) {
        if (isSongIsAlreadyOnThePlaylist(playlistId)){
            Toast.makeText(context, "This song is already in this playlist", Toast.LENGTH_SHORT).show();
        } else {
            DatabaseHelper databaseHelper = new DatabaseHelper(context);
            SQLiteDatabase database = databaseHelper.getWritableDatabase();
            databaseHelper.addSong(song.getRecentlyPlayedTitle(),song.getRecentlyPlayedDuration(), playlistId, database);
            databaseHelper.close();
            updatePlaylist(playlistId);
            dialog.dismiss();
        }
    }

    private boolean isSongIsAlreadyOnThePlaylist(int playlistId) {
        DatabaseHelper databaseHelper = new DatabaseHelper(context);
        SQLiteDatabase database = databaseHelper.getReadableDatabase();
        Cursor cursor = databaseHelper.readSong(database, playlistId);
        while (cursor.moveToNext()) {
            String title = cursor.getString(cursor.getColumnIndex(SongContract.SongEntry.COLUMN_NAME_TITLE));
            if (title.equalsIgnoreCase(song.getRecentlyPlayedTitle())){
                return true;
            }
        }
        databaseHelper.close();
        return false;
    }

    private void updatePlaylist(int playlistId) {
        int newNumber = getSongsNumberFromDatabase(playlistId);
        updateSongsNumberOnDatabase(playlistId, newNumber);
        refreshArrayList();
    }

    private int getSongsNumberFromDatabase(int position) {
        DatabaseHelper databaseHelper = new DatabaseHelper(context);
        SQLiteDatabase database = databaseHelper.getReadableDatabase();
        int newNumber = databaseHelper.songNumberForPlaylist(database, position);
        databaseHelper.close();
        return newNumber;
    }

    private void updateSongsNumberOnDatabase(int position, int newNumber) {
        DatabaseHelper databaseHelper = new DatabaseHelper(context);
        SQLiteDatabase database1 = databaseHelper.getWritableDatabase();
        databaseHelper.updatePlaylistSongNr(position, String.valueOf(newNumber), database1);
        databaseHelper.close();
    }

    private void refreshArrayList() {
        playlistList.clear();
        getPlaylistListFromDB();
        adapter.notifyDataSetChanged();
        updatePlaylistFragmentData();
    }

    private void updatePlaylistFragmentData() {
        playlistsFragment.refreshPlaylists();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == closeBtn.getId()) {
            dialog.dismiss();
        }
    }
}
