package com.example.warmup.database;

import android.provider.BaseColumns;

public class PlaylistContract {

    public PlaylistContract() {

    }

    public static class PlaylistEntry implements BaseColumns {
        public static final String TABLE_NAME = "Playlist";
        public static final String COLUMN_NAME_ID = "id";
        public static final String COLUMN_NAME_TITLE = "title";
        public static final String COLUMN_NAME_SONGS_NUMBER = "songs";
    }

}
