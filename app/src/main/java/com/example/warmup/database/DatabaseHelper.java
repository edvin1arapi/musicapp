package com.example.warmup.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import static com.example.warmup.database.PlaylistContract.PlaylistEntry.COLUMN_NAME_TITLE;

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 12;
    public static final String DATABASE_NAME = "MusicApp.db";
    public static final String CREATE_PLAYLIST_TABLE = "create table " + PlaylistContract.PlaylistEntry.TABLE_NAME + "("
            + PlaylistContract.PlaylistEntry.COLUMN_NAME_ID + " INTEGER PRIMARY KEY autoincrement,"
            + COLUMN_NAME_TITLE + " text unique,"
            + PlaylistContract.PlaylistEntry.COLUMN_NAME_SONGS_NUMBER + " text);";

    public static final String CREATE_SONG_TABLE = "create table " + SongContract.SongEntry.TABLE_NAME + "("
            + SongContract.SongEntry.COLUMN_NAME_ID + " INTEGER PRIMARY KEY autoincrement,"
            + SongContract.SongEntry.COLUMN_NAME_TITLE + " text,"
            + SongContract.SongEntry.COLUMN_NAME_DURATION + " text,"
            + SongContract.SongEntry.COLUMN_NAME_SONG_PLAYLIST_ID + " number);";

    public static final String DROP_PLAYLIST_TABLE = "drop table if exists " + PlaylistContract.PlaylistEntry.TABLE_NAME;
    public static final String DROP_SONG_TABLE = "drop table if exists " + SongContract.SongEntry.TABLE_NAME;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_PLAYLIST_TABLE);
        db.execSQL(CREATE_SONG_TABLE);
        System.out.println("Table created...");
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_PLAYLIST_TABLE);
        db.execSQL(DROP_SONG_TABLE);
        onCreate(db);
    }

    public void addPlaylist(String title, String songs, SQLiteDatabase db){
        ContentValues values = new ContentValues();
        values.put(COLUMN_NAME_TITLE, title);
        values.put(PlaylistContract.PlaylistEntry.COLUMN_NAME_SONGS_NUMBER, songs);
        db.insert(PlaylistContract.PlaylistEntry.TABLE_NAME, null, values);
        System.out.println("Record added on database");
    }

    public void addSong(String title, String duration, int playlistId, SQLiteDatabase db){
        ContentValues values = new ContentValues();
        values.put(SongContract.SongEntry.COLUMN_NAME_TITLE, title);
        values.put(SongContract.SongEntry.COLUMN_NAME_DURATION, duration);
        values.put(SongContract.SongEntry.COLUMN_NAME_SONG_PLAYLIST_ID, playlistId);
        db.insert(SongContract.SongEntry.TABLE_NAME, null, values);
    }

    public Cursor readPlaylist(SQLiteDatabase database){
        String[] projections = {PlaylistContract.PlaylistEntry.COLUMN_NAME_ID, COLUMN_NAME_TITLE, PlaylistContract.PlaylistEntry.COLUMN_NAME_SONGS_NUMBER};
        Cursor cursor = database.query(PlaylistContract.PlaylistEntry.TABLE_NAME,
                projections, null, null, null, null, null);
        return cursor;
    }

    public int songNumberForPlaylist(SQLiteDatabase database, int playlistId){
        String[] projections = {SongContract.SongEntry.COLUMN_NAME_ID};
        String selection = SongContract.SongEntry.COLUMN_NAME_SONG_PLAYLIST_ID + " == " + playlistId;
        Cursor cursor = database.query(SongContract.SongEntry.TABLE_NAME,
                projections, selection, null, null, null, null);
        return cursor.getCount();
    }

    public Cursor readSong(SQLiteDatabase database, int playlistId){
        String[] projections = {SongContract.SongEntry.COLUMN_NAME_ID, SongContract.SongEntry.COLUMN_NAME_TITLE, SongContract.SongEntry.COLUMN_NAME_DURATION};
        String selection = SongContract.SongEntry.COLUMN_NAME_SONG_PLAYLIST_ID + " == " + playlistId;
        Cursor cursor = database.query(SongContract.SongEntry.TABLE_NAME,
                projections, selection, null, null, null, null);
        return cursor;
    }

    public void deleteSong(int songId, SQLiteDatabase database){
        String selection = SongContract.SongEntry.COLUMN_NAME_SONG_PLAYLIST_ID + " == " + songId;
        database.delete(SongContract.SongEntry.TABLE_NAME, selection, null);
    }

    public void updatePlaylistTitle(int id, String title, SQLiteDatabase db){
        ContentValues values = new ContentValues();
        values.put(PlaylistContract.PlaylistEntry.COLUMN_NAME_TITLE, title);
        String selection = PlaylistContract.PlaylistEntry.COLUMN_NAME_ID + " == " + id;
        db.update(PlaylistContract.PlaylistEntry.TABLE_NAME, values, selection, null);
    }

    public void updatePlaylistSongNr(int id, String songsNr, SQLiteDatabase db){
        ContentValues values = new ContentValues();
        values.put(PlaylistContract.PlaylistEntry.COLUMN_NAME_SONGS_NUMBER, songsNr);
        String selection = PlaylistContract.PlaylistEntry.COLUMN_NAME_ID + " == " + id;
        db.update(PlaylistContract.PlaylistEntry.TABLE_NAME, values, selection, null);
    }

    public void deletePlaylist(int id, SQLiteDatabase db){
        String selection = PlaylistContract.PlaylistEntry.COLUMN_NAME_ID + " == " + id;
        db.delete(PlaylistContract.PlaylistEntry.TABLE_NAME, selection, null);

        String selection1 = SongContract.SongEntry.COLUMN_NAME_SONG_PLAYLIST_ID + " == " + id;
        db.delete(SongContract.SongEntry.TABLE_NAME, selection1, null);
    }
}
