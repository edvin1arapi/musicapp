package com.example.warmup.database;

import android.provider.BaseColumns;

public class SongContract {

    public SongContract() {

    }

    public static class SongEntry implements BaseColumns {
        public static final String TABLE_NAME = "Songs";
        public static final String COLUMN_NAME_ID = "song_id";
        public static final String COLUMN_NAME_TITLE = "song_title";
        public static final String COLUMN_NAME_DURATION = "song_duration";
        public static final String COLUMN_NAME_SONG_PLAYLIST_ID = "song_playlist_id";
    }
}
