package com.example.warmup.models;

public class RecentlyPlayedModel {

    private int recentlyPlayedImage;
    private String recentlyPlayedTitle;
    private String recentlyPlayedDuration;

    public RecentlyPlayedModel(int recentlyPlayedImage, String recentlyPlayedTitle, String recentlyPlayedDuration) {
        this.recentlyPlayedImage = recentlyPlayedImage;
        this.recentlyPlayedTitle = recentlyPlayedTitle;
        this.recentlyPlayedDuration = recentlyPlayedDuration;
    }

    public int getRecentlyPlayedImage() {
        return recentlyPlayedImage;
    }

    public void setRecentlyPlayedImage(int recentlyPlayedImage) {
        this.recentlyPlayedImage = recentlyPlayedImage;
    }

    public String getRecentlyPlayedTitle() {
        return recentlyPlayedTitle;
    }

    public void setRecentlyPlayedTitle(String recentlyPlayedTitle) {
        this.recentlyPlayedTitle = recentlyPlayedTitle;
    }

    public String getRecentlyPlayedDuration() {
        return recentlyPlayedDuration;
    }

    public void setRecentlyPlayedDuration(String recentlyPlayedDuration) {
        this.recentlyPlayedDuration = recentlyPlayedDuration;
    }

}
