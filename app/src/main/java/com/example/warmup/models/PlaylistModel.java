package com.example.warmup.models;

import java.util.List;

public class PlaylistModel {

    private int playlistId;
    private String playlistName;
    private int playlistSongsNumber;
    private List<RecentlyPlayedModel> songsList;

    public PlaylistModel(int playlistId, String playlistName, int playlistSongsNumber, List<RecentlyPlayedModel> songsList) {
        this.playlistId = playlistId;
        this.playlistName = playlistName;
        this.playlistSongsNumber = playlistSongsNumber;
        this.songsList = songsList;
    }

    public PlaylistModel() {
    }

    public int getPlaylistId() {
        return playlistId;
    }

    public void setPlaylistId(int playlistId) {
        this.playlistId = playlistId;
    }

    public String getPlaylistName() {
        return playlistName;
    }

    public void setPlaylistName(String playlistName) {
        this.playlistName = playlistName;
    }

    public int getPlaylistSongsNumber() {
        return playlistSongsNumber;
    }

    public void setPlaylistSongsNumber(int playlistSongsNumber) {
        this.playlistSongsNumber = playlistSongsNumber;
    }

    public List<RecentlyPlayedModel> getSongsList() {
        return songsList;
    }

    public void setSongsList(List<RecentlyPlayedModel> songsList) {
        this.songsList = songsList;
    }
}
