package com.example.warmup.models;

public class CategoriesModel {

    private String categoryName;
    private int categoryPhoto;

    public CategoriesModel(String CategoryName, int categoryPhoto) {
        this.categoryName = CategoryName;
        this.categoryPhoto = categoryPhoto;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public int getCategoryPhoto() {
        return categoryPhoto;
    }

    public void setCategoryPhoto(int categoryPhoto) {
        this.categoryPhoto = categoryPhoto;
    }
}
