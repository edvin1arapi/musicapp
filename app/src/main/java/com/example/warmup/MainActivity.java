package com.example.warmup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;
import android.view.MenuItem;

import com.example.warmup.fragments.ExploreFragment;
import com.example.warmup.fragments.PlayMusicFragment;
import com.example.warmup.fragments.PlaylistsFragment;
import com.example.warmup.fragments.SettingsFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity implements PlayMusicFragment.Communicator {

    private BottomNavigationView bottomNavigationView;
    private final PlaylistsFragment playlistsFragment = new PlaylistsFragment();
    private final ExploreFragment exploreFragment = new ExploreFragment(playlistsFragment);
    private final SettingsFragment settingsFragment = new SettingsFragment();
    final FragmentManager fragmentManager = getSupportFragmentManager();
    public Fragment active = exploreFragment;
    private boolean shown;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViews();
        setOnClickListeners();
        createFragments();
    }

    private void createFragments() {
        fragmentManager.beginTransaction().add(R.id.main_container, settingsFragment, "3").hide(settingsFragment).commit();
        fragmentManager.beginTransaction().add(R.id.main_container, playlistsFragment, "2").hide(playlistsFragment).commit();
        fragmentManager.beginTransaction().add(R.id.main_container, exploreFragment, "1").commit();
    }

    private void findViews() {
        bottomNavigationView = findViewById(R.id.bottom_navigation);
    }

    private void setOnClickListeners() {
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_explore:
                    fragmentManager.beginTransaction().hide(active).show(exploreFragment).commit();
                    active = exploreFragment;
                    return true;

                case R.id.navigation_playlists:
                    fragmentManager.beginTransaction().hide(active).show(playlistsFragment).commit();
                    active = playlistsFragment;
                    return true;

                case R.id.navigation_settings:
                    fragmentManager.beginTransaction().hide(active).show(settingsFragment).commit();
                    active = settingsFragment;
                    return true;
            }
            return false;
        }
    };

    @Override
    public void doChanges(int end, boolean shown) {
        playlistsFragment.onMusicPlaying(end);
        setDialogShown(shown);
    }

    public FragmentManager getManager() {
        return fragmentManager;
    }

    public void setDialogShown(boolean shown) {
        this.shown = shown;
    }

    public boolean isDialogShown() {
        return shown;
    }
}
